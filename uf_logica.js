//Asignacion de evento para la tecla "enter"
function PulsarTecla(e) {
    var e = e || event;
    var tecla = e.keyCode;

    if (tecla == 13) {
        $('#enviar').click();
    }
}
//En espera al evento
document.onkeydown = PulsarTecla;

function consultaUF() {
    //Consultando a la API a partir de los parámetros ingresados
    var anio = parseInt(document.getElementById("entanio").value);
    var mes = parseInt(document.getElementById("entmes").value);

    fetch(`https://api.sbif.cl/api-sbifv3/recursos_api/uf/${anio}/${mes}?apikey=f5a27609b8eb2f4c5eecb0d2c441231c4cffe6da&formato=json`)
        .then(respuesta => respuesta.json())
        .then(data => {
            let html = '';
            if (!data.UFs) {
                //Lanzamiento de error en caso de ingreso de parámetros sin datos
                alert("Error: Parámetros incorrectos! \n \n Año: 1990 - 2019 \n Mes: 01 - 12");
                entanio.value = "";
                entmes.value = "";
            }
            else {
                //Impresión de los datos en el documento
                entanio.value = "";
                entmes.value = "";
                data.UFs.forEach(resultado => {
                    html += `<ul style="list-style-type:none;" id='lista'><li style = "color: white; line-height: 0;">Fecha: ${resultado.Fecha} / UF: $${resultado.Valor}</li></ul>`;
                });
                document.getElementById("imp").innerHTML = html;
            }
        });
}
